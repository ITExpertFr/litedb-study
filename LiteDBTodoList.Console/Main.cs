﻿using System;
using LiteDB;
using LiteDBTodoList.Console.Dtos;
using LiteDBTodoList.Console.Entities;
using LiteDBTodoList.Console.Services;

namespace LiteDBTodoList.Console
{
    public class Main
    {
        readonly TodoListRepository TodoListRepository;

        public Main()
        {
            LiteDatabase db = new(@"Database.db");
            TodoListRepository = new(db);
        }

        public void Invoke(CommandLineArgument arg)
        {
            switch (arg.Command)
            {
                case "add":
                {
                    var insertedTodo = TodoListRepository.Add(new Todo
                    {
                        Title = arg.TodoTitle
                    });
                    PrintTodo(insertedTodo);
                } break;

                case "remove":
                {
                    var removedTodo = TodoListRepository.Remove(arg.TodoId);
                    PrintTodo(removedTodo);
                } break;

                case "update":
                {
                    var updatedTodo = TodoListRepository.Update(arg.TodoId, new Todo
                    {
                        Title = arg.TodoTitle,
                    });
                    PrintTodo(updatedTodo);
                } break;

                case "complete":
                {
                    var completedTodo = TodoListRepository.Update(arg.TodoId, new()
                    {
                        IsDone = true,
                    });
                    PrintTodo(completedTodo);
                } break;

                case "list":
                {
                    var todos = TodoListRepository.GetOngoing();
                    foreach (var todo in todos)
                    {
                        PrintTodo(todo);
                    }
                } break;

                case "get":
                {
                    var todo = TodoListRepository.Get(arg.TodoId);
                    PrintTodo(todo);
                } break;
            }
        }

        static void PrintTodo(Todo todo)
        {
            if (todo == null)
                return;

            if (todo.IsDone)
            {
                System.Console.ForegroundColor = ConsoleColor.Gray;
                System.Console.WriteLine(
                    $"{todo.Id}. {todo.Title} ({todo.CreationDate:dd/MM/yyyy}) - Done ({todo.DoneDate:dd/MM/yyyy})");
                System.Console.ForegroundColor = ConsoleColor.White;
            }
            else
                System.Console.WriteLine(
                    $"{todo.Id}. {todo.Title} ({todo.CreationDate:dd/MM/yyyy})");
        }
    }
}
