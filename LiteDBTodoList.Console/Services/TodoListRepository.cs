﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using LiteDB;
using LiteDBTodoList.Console.Entities;

namespace LiteDBTodoList.Console.Services
{
    public class TodoListRepository
    {
        readonly ILiteCollection<Todo> Table;
        static int Serial;

        public TodoListRepository(ILiteDatabase db)
        {
            Table = db.GetCollection<Todo>("todos");
            Table.EnsureIndex(x => x.Id);

            if (GetAll().Any())
                Serial = Table.Max(x => x.Id) + 1;
            else
                Serial = 1;
        }

        public Todo Add(Todo todo)
        {
            todo.Id = Serial;
            todo.IsDone = false;
            todo.CreationDate = DateTime.Now;

            Table.Insert(todo);

            Serial++;

            return todo;
        }

        public Todo Remove(int todoId)
        {
            var todoToRemove = Get(todoId);

            if (todoToRemove == null)
                return null;

            if (Table.Delete(todoId))
                return todoToRemove;

            throw new("Delete didn't work :sad:");
        }

        public Todo Update(int todoId, Todo todo)
        {
            var todoToUpdate = Get(todoId);

            if (todoToUpdate == null)
                return null;

            todoToUpdate.Title = todo.Title ?? todoToUpdate.Title;
            todoToUpdate.IsDone = todo.IsDone;
            todoToUpdate.DoneDate = todo.IsDone ? DateTime.Now : null;

            if (Table.Update(todoToUpdate))
                return todoToUpdate;

            throw new("Update didn't work :(");
        }

        public Todo Get(int todoId)
        {
            return Table.Query().Where(x => x.Id == todoId).FirstOrDefault();
        }

        public IEnumerable<Todo> GetAll()
        {
            return Table.Query().ToEnumerable();
        }

        public IEnumerable<Todo> GetOngoing()
        {
            return Table.Query().Where(x => !x.IsDone).ToEnumerable();
        }
    }
}
