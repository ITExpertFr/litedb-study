﻿namespace LiteDBTodoList.Console.Constants
{
    public static class Commands
    {
        public const string Add = "add";
        public const string Remove = "remove";
        public const string Update = "update";
        public const string Complete = "complete";
        public const string List = "list";
        public const string Get = "get";

        public static bool CommandNeedTitle(string arg)
            => arg is Add or Update;

        public static bool CommandNeedId(string arg)
            => arg is Remove or Update or Complete or Get;
    }
}
