﻿using System;
using LiteDBTodoList.Console.Constants;
using LiteDBTodoList.Console.Dtos;

namespace LiteDBTodoList.Console
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            if (args.Length == 0)
                throw new ArgumentException();

            CommandLineArgument command;

            if (Commands.CommandNeedId(args[0]))
            {
                if (Commands.CommandNeedTitle(args[0]))
                    command = new(args[0], int.Parse(args[1]), args[2]);
                else
                    command = new(args[0], int.Parse(args[1]));
            }
            else if (Commands.CommandNeedTitle(args[0]))
                command = new(args[0], args[1]);
            else
                command = new(args[0]);

            var program = new Main();
            program.Invoke(command);
        }
    }
}
