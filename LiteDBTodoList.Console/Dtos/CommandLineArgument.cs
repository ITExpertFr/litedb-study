﻿namespace LiteDBTodoList.Console.Dtos
{
    public class CommandLineArgument
    {
        public CommandLineArgument(string command)
        {
            Command = command;
        }

        public CommandLineArgument(string command, int todoId)
        {
            Command = command;
            TodoId = todoId;
        }

        public CommandLineArgument(string command, string todoTitle)
        {
            Command = command;
            TodoTitle = todoTitle;
        }

        public CommandLineArgument(string command, int todoId, string todoTitle)
        {
            Command = command;
            TodoId = todoId;
            TodoTitle = todoTitle;
        }

        public string Command { get; }
        public string TodoTitle { get; }
        public int TodoId { get; }
    }
}
