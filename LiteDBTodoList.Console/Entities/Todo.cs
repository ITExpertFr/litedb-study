﻿using System;

namespace LiteDBTodoList.Console.Entities
{
    public class Todo
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public bool IsDone { get; set; }
        public DateTime? DoneDate { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
